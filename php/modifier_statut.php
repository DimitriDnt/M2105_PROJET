<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8"/>
        	<link rel="stylesheet" href="../css/basic.css"/>
		<title>Emprunt</title>
		<?php include('../bdd/connect.php'); ?>
	</head>

	<body>

		<center>

			<form method="POST" action="modifier_traitement.php">

				<?php

				// Si le statut d'un matériel est sur "Emprunté" alors un autre utilisateur ne pourra pas l'emprunter

				if ($_GET['statut'] == "Emprunté")
				{
					header('Location: liste_materiel_user.php');
				}

				else
				{
					echo'
					<input type="hidden" value='.$_GET['id_materiel'].' name="id_materiel">
					<br>
					
					<h4>Veuillez entrer votre nom :</h4>
					<input name="nom" value='.$_GET['nom'].'>';
				}

				?>

				<br/>
				<br/>
				<input type="submit" name="envoyer" value="Confirmer l'emprunt">                                               
			</form>

		</center>
</html>