<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8"/>
        	<link rel="stylesheet" href="../css/basic.css"/>
		<title>Supprimer</title>
		<?php include('../bdd/connect.php');?>
	</head>

	<body>
		<center>
			<?php

				$id_materiel = $_GET['id_materiel'];
				
				// Cette requête permet de supprimer une ligne du tableau
				$requete = $bdd->exec("DELETE FROM materiel WHERE id_materiel = '$id_materiel' ");

				if ($requete)
				{
					echo '<br>';
					echo ("Suppression effectuée.");
				}

				else
				{
					echo("La modification a echouée !");
				}
			?>
		<br>
	
		<a href="liste_materiel.php">Retour à la liste du matériel</a>

	</center>

	</body>

</html>