<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
        	<link rel="stylesheet" href="../style/basic.css"/>
		<title>Traitement</title>
		<?php include('../bdd/connect.php');?>
	</head>

	<body>
		<center>
			<?php

			// On prépare les requêtes SQL pour récupérer les noms d'utilisateurs et les mot de passes

			$sql1 = $bdd->query("SELECT nom_utilisateur FROM utilisateurs WHERE type_utilisateur='Admin'");
			$sql2 = $bdd->query("SELECT mdp_utilisateur FROM utilisateurs WHERE type_utilisateur='Admin'");

			// Lorsque l'administrateur se connectera avec ses identifiants, il sera re-dirigé sur une page

			while ($donnees1 = $sql1->fetch() AND $donnees2 = $sql2->fetch())
			{
				if ($_POST['nom'] == $donnees1['nom_utilisateur'] AND $_POST['mdp'] == $donnees2['mdp_utilisateur'])
				{
					header('Location: ajouter_materiel.php?statut=$s&id_materiel=$i&type_materiel=$t&nom=$n');
				}

				// Si les identifiants saisis sont incorrects, il y aura une redirection sur la page de connexion

				else {

					echo '<br>';

					header('Location: login.php?auth=err');
				}

			}

			?>

		</center>
	</body>
</html>