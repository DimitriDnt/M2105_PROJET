<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
        	<link rel="stylesheet" href="../style/basic.css"/>
		<title>Traitement</title>
		<?php include('../bdd/connect.php');?>
	</head>

	<body>
		<center>
			<?php
				// On execute la requête pour ajouter un matériel et on ajoute les champs saisies dans la base de données

				$requete = $bdd->prepare('INSERT INTO materiel (id_materiel, type_materiel, statut, nom) VALUES (?,?,"Disponible",?)');
				$requete->execute(array($_POST['id_materiel'], $_POST['type_materiel'], $_POST['nom'])); 

				// Test si la requête a bien fonctionné

				if ($requete)
					{
						echo '<br>';
						echo ("Le matériel a bien été ajouté.");
					}

				// On envoie un message d'erreur dans le cas contraire
					
					else
					{
						echo("La modification a echouée !");
					}
			?>

			<br>
			<a href="liste_materiel.php">Retour à la liste du matériel</a>
		</center>
	</body>

</html>