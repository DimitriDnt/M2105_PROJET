<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="../css/basic.css" />
		<title>Liste du materiel</title>
		<?php include("../bdd/connect.php");?>
	</head>

	<body>

			<center>

				<h1>Liste du matériel</h1>
				<a href="liste_materiel_user.php">Accueil</a>

				<form method="GET" action="modifier_statut.php">

					<table>

						<?php
							echo '<tr>
							<td><b>ID</b></td>
							<td><b>TYPE</b></td>
							<td><b>STATUT</b></td>
							<td><b>NOM</b></td>
							<td><b>EMPRUNT</b></td>
							<td><b>RETOUR</b></td>
							</tr>';

							$sql = ("SELECT id_materiel, type_materiel, statut, nom FROM materiel");

							$reponse = $bdd->query($sql);

							// On affiche les données contenues dans la table materiel
							
							while ($donnees = $reponse->fetch())
								
								{
									$i = $donnees["id_materiel"];
									$t = $donnees["type_materiel"];
									$s = $donnees["statut"];
									$n = $donnees["nom"];

									echo 
									"<tr><td>".$donnees["id_materiel"].
									"</td><td>".$donnees["type_materiel"].
									"</td><td>".$donnees["statut"].
									"</td><td>".$donnees["nom"].
									"</td><td><a href=\"modifier_statut.php?statut=$s&id_materiel=$i&nom=$n\">Emprunter</a>
									<td><a href=\"retour.php?statut=$s&id_materiel=$i&nom=$n\">Retourner</a></td></tr>".
									'<br>';

								}

							$reponse->closeCursor(); // Termine la requête en cours

						?>

					</table>
					<br>

				</form>
				<br>

				<a href="login.php">Ajouter matériel</a>

			</center>

	</body>

</html>